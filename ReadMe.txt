Jeu de Memory HTML / CSS / JS / PHP POO

Ce projet a été conçu comme un support pédagogique progressif. On peut montrer le produit fini aux élèves en leur expliquant qu'ils vont devoir le refaire. Ils pourront commencer par une phase d'analyse "sur papier" pour poser les éléments d'architecture simples. Ensuite, on pourra leur montrer les différentes parties du code pour leur expliquer notre approche. Après, on pourra explorer le code en mode debug pas-à-pas afin qu'ils comprennent les mécanismes. Enfin, on leur donne le code pour qu'ils le complète, le refactorise et le corrige si besoin.
Le projet aborde les points suivants :
- analyse : une toute petite partie de la documentation (conception et inline) existe. Il faut la compléter.
- HTML / CSS : les bases de la présentation des images
- JS : les bases de l'interactivité avec l'utilisateur
- PHP : le typage strict, et la POO en montrant les notions de
    ° encapsulation
    ° héritage
    ° composition
    ° classe abstraite
- SQL : le CRUD de base
- les interactions avec l'utilisateur
- le protocole HTTP
- les interactions entre l'interface utilisateur, le "métier" et la base de données
- architecture du code : ce projet explore les bases de ce que pourrait être un MVC si on refactorise une partie. Peut permettre d'introduire les concepts de base qu'on retrouve dans des frameworks comme Symfony ou Laravel.
- sécurité : le code essaye de montrer des bonnes pratiques, mais toutes ne sont pas mises en oeuvre. Il faut expliquer aux élèves où sont les manques et comment les combler pour leur permettre de le faire eux-mêmes quand ils reprendront le code.
- qualité : c'est-à-dire clarté + sémantique + documentation + architecture + sécurité du code

Ce qu'il reste à faire aux élèves :
- compléter l'analyse et sa documentation
- rendre le rendu web responsive, et ajouter les éléments manquants
- refactoriser et compléter le code pour le rendre plus clair et mieux structuré
- compléter la documentation inline
- écrire les tests unitaires
- porter la base de donnée sur un autre type de serveur

C'est aussi bien sûr l'occasion d'aborder et d'approfondir la gestion de projet et le versionning à plusieurs.