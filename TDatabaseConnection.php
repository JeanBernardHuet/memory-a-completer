<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * PHP 7.4 is the minimum version to run the code below.
 *
 * As this class is designed to managed data coming from outside its own code, the correct posture is: TRUST NO ONE! (and check everything)
 */

// Strong typing is a good basis for security
declare(strict_types = 1);

/**
 * TDatabaseConnection is the parent (generic) class for any specific database class.
 *
 * TDatabaseConnection class has fields and methods to manage connections to a database.
 * 3 parameters are required:
 *   - INI file (full path + file name) that contains informations to connect
 *   - database server type. Possible values:
 *     ° self::SRV_EMBEDDED (for databases stored in files like SQLite and Firebird). Default value.
 *     ° self::SRV_LOCAL (for databases server on local computer)
 *     ° self::SRV_REMOTE (for databases server over the network)
 *   - access level on database. Possible values:
 *     ° self::DB_ROOT (using account with root privilege on database)
 *     ° self::DB_ADMIN (using account with admin - full - privilege on database)
 *     ° self::DB_RW (using account with read and write - SELECT, INSERT, UPDATE , DELETE - privileges on database)
 *     ° self::DB_RW (using account with write - INSERT, UPDATE , DELETE - only privilege on database)
 *     ° self::DB_RO (using account with read - SELECT - only privilege on database)
 *     ° self::DB_NO (no account needed - Should be only for SQLite). Default value.
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 *
 * @property-read bool $IsConnected Connection status
 * @property-read \PDO $Database Database PDO object
 */
abstract class TDatabaseConnection {

    protected const SRV_EMBEDDED = 'embedded';
    protected const SRV_LOCAL    = 'local';
    protected const SRV_REMOTE   = 'remote';
    protected const DB_ROOT      = 'root';
    protected const DB_ADMIN     = 'admin';
    protected const DB_RW        = 'read_write';
    protected const DB_WO        = 'write_only';
    protected const DB_RO        = 'read_only';
    protected const DB_NO        = 'not_used';

    /** @var string $FDbServerType Contains one of the correct database server values: SELF::SRV_EMBEDDED, SELF::SRV_LOCAL or SELF::SRV_REMOTE. Default value is self::SRV_EMBEDDED. */
    protected string $FDbServerType = SELF::SRV_EMBEDDED;

    /**
     *
     * @var string $FDbAccessLevel
     */
    protected string $FDbAccessLevel = SELF::DB_NO;

    /**
     *
     * @var array $FIniFileContent
     */
    private array $FIniFileContent = [
            ];

    /**
     *
     * @var string $FDbDriver
     */
    protected string $FDbDriver = '';

    /**
     *
     * @var string $FDbHost
     */
    protected string $FDbHost = '';

    /**
     *
     * @var string $FDbSchema
     */
    protected string $FDbSchema = '';

    /**
     *
     * @var string $FDbUserName
     */
    protected string $FDbUserName = '';

    /**
     *
     * @var string $FDbUserPassword
     */
    protected string $FDbUserPassword = '';

    /**
     * FDatabase is the PDO object managing access to database.
     *
     * @var \PDO|null $FDatabase Default is NULL.
     */
    protected ?\PDO $FDatabase = NULL;

    /**
     * FIsConnected represents connection to database status. Should always be equal to: !(null == $this->FDatabase).
     *
     * @var bool $FIsConnected Default is FALSE.
     */
    protected bool $FIsConnected = FALSE;

    /**
     *
     */
    abstract public function connect(
            string $AIniFilePath, string $AIniFileName, string $ADbServerType,
            string $AAccessLevel
    ): bool;

    /**
     * connect () close connection to database.
     *
     * @return void None returned because... well... NULL is NULL, anyhow...
     */
    public function disconnect(): void {
        $this->FDatabase = NULL;
    }

    /**
     *
     * @param string $ADbServer
     * @return void
     * @throws \Exception
     */
    protected function checkFDbServerType(string $ADbServer = SELF::SRV_EMBEDDED): void {
        switch (strtolower($ADbServer)) {
            case SELF::SRV_EMBEDDED:
                // case SELF::SRV_LOCAL:
                // case SELF::SRV_REMOTE:
                $this->FDbServerType = strtolower($ADbServer);
                break;
            default:
                $LogTime             = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** Parameter value error - Incorrect database server name parameter. Value send = ' . $ADbServer . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Incorrect database server name parameter.');
                break;
        }
    }

    /**
     *
     * @param string $AAccessLevel
     * @return void
     * @throws \Exception
     */
    protected function checkFDbAccessLevel(string $AAccessLevel = SELF::DB_NO): void {
        switch (strtolower($AAccessLevel)) {
            // case self::DB_ROOT:
            // case self::DB_ADMIN:
            // case self::DB_RW:
            // case self::DB_WO:
            // case self::DB_RO:
            case SELF::DB_NO:
                $this->FDbAccessLevel = strtolower($AAccessLevel);
                break;
            default:
                $LogTime              = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** Parameter value error - Incorrect access level to database parameter. Value send = ' . $AAccessLevel . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Incorrect access level to database parameter.');
                break;
        }
    }

    /**
     *
     * @param string $AIniFilePath
     * @param string $AIniFileName
     * @return void
     * @throws \Exception
     */
    private function checkFIniFileContent(string $AIniFilePath = '',
                                          string $AIniFileName = ''): void {
        if (DIRECTORY_SEPARATOR != substr($AIniFilePath, -1)) {
            $AIniFilePath = $AIniFilePath . DIRECTORY_SEPARATOR;
        }
        /** @var array|bool $DbSettings */
        if ( ! $DbSettings = parse_ini_file(
                $AIniFilePath . $AIniFileName, true
                )) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** File error - INI file not found. Path given: ' . $AIniFilePath . ' File name given: ' . $AIniFileName . ' Check path and file name.' . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Unable to read database connection settings.');
        }
        else {
            $this->FIniFileContent = $DbSettings;
        }
    }

    /**
     *
     * @return void
     * @throws \Exception
     */
    private function checkDbDriver(): void {
        if (isset($this->FIniFileContent[$this->FDbServerType . '_db_server']['driver'])) {
            $this->FDbDriver = $this->FIniFileContent[$this->FDbServerType . '_db_server']['driver'];
        }
        else {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** File error - Database driver value not found in INI file. File content = ' . print_r($this->FIniFileContent) . PHP_EOL,
                                                                                                                                                    3,
                                                                                                                                                    __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Unknown database driver.');
        }
    }

    /**
     *
     * @return void
     * @throws \Exception
     */
    private function checkDbHost(): void {
        if (isset($this->FIniFileContent[$this->FDbServerType . '_db_server']['host'])) {
            $this->FDbHost = $this->FIniFileContent[$this->FDbServerType . '_db_server']['host'];
        }
        else {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** File error - Database host value not found in INI file. File content = ' . print_r($this->FIniFileContent) . PHP_EOL,
                                                                                                                                                  3,
                                                                                                                                                  __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Unknown host.');
        }
    }

    /**
     *
     * @return void
     * @throws \Exception
     */
    private function checkDbSchema(): void {
        if (isset($this->FIniFileContent[$this->FDbServerType . '_db_schema']['schema'])) {
            $this->FDbSchema = $this->FIniFileContent[$this->FDbServerType . '_db_schema']['schema'];
        }
        else {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** File error - Database schema value not found in INI file. File content = ' . print_r($this->FIniFileContent) . PHP_EOL,
                                                                                                                                                    3,
                                                                                                                                                    __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Unknown database.');
        }
    }

    /**
     *
     * @return void
     * @throws \Exception
     */
    private function checkDbUserName(): void {
        if (isset($this->FIniFileContent[$this->FDbServerType . '_db_' . $this->FDbAccessLevel]['username'])) {
            $this->FDbUserName = $this->FIniFileContent[$this->FDbServerType . '_db_' . $this->FDbAccessLevel]['username'];
        }
        else {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** File error - Database user name value not found in INI file. File content = ' . print_r($this->FIniFileContent) . PHP_EOL,
                                                                                                                                                       3,
                                                                                                                                                       __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Unknown user or password.');
        }
    }

    /**
     *
     * @return void
     * @throws \Exception
     */
    private function checkDbUserPassword(): void {
        if (isset($this->FIniFileContent[$this->FDbServerType . '_db_' . $this->FDbAccessLevel]['password'])) {
            $this->FDbUserPassword = $this->FIniFileContent[$this->FDbServerType . '_db_' . $this->FDbAccessLevel]['password'];
        }
        else {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** File error - Database user password value not found in INI file. File content = ' . print_r($this->FIniFileContent) . PHP_EOL,
                                                                                                                                                           3,
                                                                                                                                                           __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Unknown user or password.');
        }
    }

    /**
     *
     * @param string $AIniFilePath
     * @param string $AIniFileName
     * @return void
     */
    protected function fillDbConnectionFields(string $AIniFilePath,
                                              string $AIniFileName): void {
        // Checking if INI file exists
        $this->checkFIniFileContent($AIniFilePath, $AIniFileName);

        // Checking if driver value exists
        $this->checkDbDriver();

        // Checking if host value exists
        $this->checkDbHost();

        if (SELF::SRV_EMBEDDED != $this->FDbServerType) {
            // Checking if schema value exists
            $this->checkDbSchema();
        }
        if (SELF::DB_NO != $this->FDbAccessLevel) {
            // Checking if username exists
            $this->checkDbUserName();

            //Checking if password exists
            $this->checkDbUserPassword();
        }
    }

    /**
     *
     * @param string $PseudoAttribueName
     * @return mixed
     */
    public function __get(string $PseudoAttribueName) {
        switch ($PseudoAttribueName) {
            case 'IsConnected':
                return $this->FIsConnected;
                break;
            case 'Database':
                return $this->FDatabase;
            default:
                break;
        }
    }

}
