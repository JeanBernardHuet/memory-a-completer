<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
declare(strict_types = 1);

/**
 * Description of TGameLevel
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
class TGameLevel {

    private \PDO $FDbManager;

    /**
     *
     * @param \PDO $ADbManager
     */
    public function __construct(\PDO $ADbManager) {
        $this->FDbManager = $ADbManager;
    }

    /**
     *
     *
     * @return array
     * @throws \Exception
     */
    public function readFullLevelList(): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_game_level');
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_game_level' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading full level list request.');
            }
        }
        catch (\PDOException $pdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_game_level. Error detail = ' . $pdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read full level list.');
        }
    }

    public function readDurationById(int $AId): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT duration FROM t_game_level WHERE id = :id');
            $Request->bindValue(':id', $AId, PDO::PARAM_INT);
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT duration FROM t_game_level WHERE id = :id' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading duration by id request.');
            }
        }
        catch (\PDOException $pdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT duration FROM t_game_level WHERE id = :id. Error detail = ' . $pdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read duration by id.');
        }
    }

}
