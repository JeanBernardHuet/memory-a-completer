<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
declare(strict_types = 1);

/**
 * Description of TGameManager
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
class TGameManager {

    private \TMemoryDatabase $FGameDb;
    private \TBoardGame $FBoard;
    private \TCurrentGame $FGameParams;
    private \TGameLevel $FGameLevel;
    private \TGamesHistory $FGamesHistory;
    private \TGameView $FBoardDisplay;

    public function __construct() {
        include_once 'TMemoryDatabase.php';
        include_once 'TBoardGame.php';
        include_once 'TCurrentGame.php';
        include_once 'TGameLevel.php';
        include_once 'TGamesHistory.php';
        include_once 'TGameView.php';
        $this->FGameDb       = new TMemoryDatabase();
        $this->FBoard        = new TBoardGame($this->FGameDb->Database);
        $this->FGameParams   = new TCurrentGame($this->FGameDb->Database);
        $this->FGameLevel    = new TGameLevel($this->FGameDb->Database);
        $this->FGamesHistory = new TGamesHistory($this->FGameDb->Database);
        $this->FBoardDisplay = new TGameView();
    }

    public function newGame(): void {
        $this->FGameParams->clean();
        $this->FBoard->newBoard();
        $this->FBoardDisplay->displayHeader();
        $this->FBoardDisplay->displayBoard($this->FBoard->readFullList());
        $this->FBoardDisplay->displayFooter();
    }

    public function checkTypeCard(int $ACardId): string {
        (array) $Card = $this->FBoard->readCardByOrder($ACardId);
        if (isset($Card[0]['card_type'])) {
            return strval($Card[0]['card_type']);
        }
        else {
            return 'NULL';
        }
    }

    public function firstCardId(): int {
        (array) $CardId = $this->FGameParams->readByKey('firstCardId');
        if (isset($CardId[0]['game_value'])) {
            return intval($CardId[0]['game_value']);
        }
        else {
            return -1;
        }
    }

    public function readCardById(int $AId): array {
        return $this->FBoard->readCardById($AId);
    }

    public function resetFirstCard(): void {
        $this->FGameParams->deleteByKey('firstCardId');
    }

    public function storeFirstCardId(int $AOrder) {
        (array) $Card = $this->FBoard->readCardByOrder($AOrder);
        if (isset($Card[0]['id'])) {
            $this->FGameParams->createKeyValuePair('firstCardId',
                                                   strval($Card[0]['id']));
        }
    }

    public function storePairFound(int $AType) {
        $this->FBoard->updatePairFoundByType($AType, TRUE);
    }

    public function isGameWon(): bool {
        return $this->FBoard->readNotFoundCards();
    }

    public function storeStartGame(): void {
        if ( ! $this->FGameParams->readByKey('startTime')) {
            $StartTime = new \DateTime('now');
            $this->FGameParams->createKeyValuePair('startTime',
                                                   $StartTime->format('Y-m-d H:i:s'));
        }
    }

    public function storeGameLevel(): void {
        if ( ! $this->FGameParams->readByKey('gameLevel')) {
            $this->FGameParams->createKeyValuePair('gameLevel', '1');
        }
    }

    public function timeIsUp(): bool {
        $CurrentTime = new \DateTime('now');
        $StartTime   = $this->FGameParams->readByKey('startTime');
        if ($StartTime) {
            $EndTime = new \DateTime($StartTime);
        }
        $EndTime->add(new DateInterval('10i'));
        return ($CurrentTime > $EndTime);
    }

    public function storeGameHistory() {
        $CurrentTime = new \DateTime('now');
        $StartTime   = $this->FGameParams->readByKey('startTime');
        if ($StartTime) {
            $EndTime = new \DateTime($StartTime);
        }
        $EndTime->sub($CurrentTime);
        $this->FGamesHistory->insertGameHistory(1, $EndTime);
    }

}
