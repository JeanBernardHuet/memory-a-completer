<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
declare(strict_types = 1);

/**
 * Description of TCurrentGame
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
class TCurrentGame {

    private \PDO $FDbManager;

    /**
     *
     * @param \PDO $ADbManager
     */
    public function __construct(\PDO $ADbManager) {
        $this->FDbManager = $ADbManager;
    }

    /**
     *
     * @return array
     * @throws \Exception
     */
    public function readFullList(): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_current_game');
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_current_game' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading full current game list request.');
            }
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_current_game. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read full current game table.');
        }
    }

    /**
     *
     * @param int $AId
     * @return array
     * @throws \Exception
     */
    public function readById(int $AId): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_current_game WHERE id = :id');
            $Request->bindValue(':id', $AId, PDO::PARAM_INT);
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_current_game WHERE id = :id' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading current game list by id request.');
            }
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_current_game WHERE id = :id. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read current game value by id.');
        }
    }

    /**
     *
     * @param string $AKey
     * @return array
     * @throws \Exception
     */
    public function readByKey(string $AKey): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_current_game WHERE game_key = :game_key');
            $Request->bindValue(':game_key', $AKey, PDO::PARAM_STR);
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_current_game WHERE game_key = :game_key' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading current game list by key request.');
            }
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_current_game WHERE key = :key. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read current game value by key.');
        }
    }

    /**
     *
     * @param string $AKey
     * @param string $AValue
     * @return bool
     * @throws \Exception
     */
    public function createKeyValuePair(string $AKey, string $AValue): bool {
        try {
            $Request = $this->FDbManager->prepare('INSERT INTO t_current_game (game_key, game_value) VALUES (:key, :value)');
            $Request->bindValue(':key', $AKey, PDO::PARAM_STR);
            $Request->bindValue(':value', $AValue, PDO::PARAM_STR);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing INSERT INTO t_current_game (game_key, game_value) VALUES (:key, :value). Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not insert key-value pair in current game table.');
        }
    }

    /**
     *
     * @param int $AID
     * @param string $AValue
     * @return bool
     * @throws \Exception
     */
    public function updateById(int $AId, string $AValue): bool {
        try {
            $Request = $this->FDbManager->prepare('UPDATE t_current_game SET game_value = :value WHERE id = :id');
            $Request->bindValue(':id', $AId, PDO::PARAM_INT);
            $Request->bindValue(':value', $AValue, PDO::PARAM_STR);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing UPDATE t_current_game SET game_value = :value WHERE id = :id. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not update value by id in current game table.');
        }
    }

    /**
     *
     * @param string $AKey
     * @param string $AValue
     * @return bool
     * @throws \Exception
     */
    public function updateByKey(string $AKey, string $AValue): bool {
        try {
            $Request = $this->FDbManager->prepare('UPDATE t_current_game SET game_value = :value WHERE game_key = :key');
            $Request->bindValue(':key', $AKey, PDO::PARAM_STR);
            $Request->bindValue(':value', $AValue, PDO::PARAM_STR);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing UPDATE t_current_game SET game_value = :value WHERE game_key = :key. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not update value by key in current game table.');
        }
    }

    /**
     *
     * @param int $AId
     * @return bool
     * @throws \Exception
     */
    public function deleteById(int $AId): bool {
        try {
            $Request = $this->FDbManager->prepare('DELETE FROM t_current_game WHERE id = :id');
            $Request->bindValue(':id', $AId, PDO::PARAM_INT);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing DELETE FROM t_current_game WHERE id = :id. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not delete key-value pair by id in current game table.');
        }
    }

    /**
     *
     * @param string $AKey
     * @return bool
     * @throws \Exception
     */
    public function deleteByKey(string $AKey): bool {
        try {
            $Request = $this->FDbManager->prepare('DELETE FROM t_current_game WHERE game_key = :key');
            $Request->bindValue(':key', $AKey, PDO::PARAM_STR);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing DELETE FROM t_current_game WHERE game_key = :key. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not delete key-value pair by key in current game table.');
        }
    }

    /**
     *
     * @return bool
     * @throws \Exception
     */
    public function clean(): bool {
        try {
            $Request = $this->FDbManager->prepare('DELETE FROM t_current_game');
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing TRUNCATE TABLE t_current_game. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not clean current game table.');
        }
    }

}
