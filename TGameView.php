<?php
/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
declare(strict_types = 1);

/**
 * Description of TGameView
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
class TGameView {

    /**
     *
     */
    public function displayHeader() {
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <title>Memory game</title>
                <meta charset="UTF-8">
                <link rel="stylesheet" href="./inc/style.css">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
            </head>
            <body>
                <?php
            }

            /**
             *
             */
            public function displayFooter() {
                ?>
                <script src="./inc/scripts.js"></script>
            </body>
        </html>
        <?php
    }

    /**
     *
     * @param array $ABoard
     */
    public function displayBoard(array $ABoard) {
        foreach ($ABoard as $Card) {
            if (isset($Card['card_type']) && isset($Card['card_order']) && isset($Card['pair_found'])) {
                $CardId      = sprintf('c%s', $Card['card_order']);
                $CardDisplay = 'card flipped';
                if ($Card['pair_found']) {
                    $CardDisplay = 'card visible ' . sprintf('type%s',
                                                             $Card['card_type']);
                }
                ?>
                <div id="<?php print($CardId); ?>" class="<?php print $CardDisplay; ?>"></div>
                <?php
            }
        }
    }

}
