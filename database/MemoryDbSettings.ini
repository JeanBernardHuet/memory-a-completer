; -------------------------------------------------------------------------------
; - Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com                           -
; -                                                                             -
; - This program is free software; you can redistribute it and/or               -
; - modify it under the terms of the GNU General Public License                 -
; - as published by the Free Software Foundation; either version 2              -
; - of the License, or (at your option) any later version.                      -
; -                                                                             -
; - This program is distributed in the hope that it will be useful,             -
; - but WITHOUT ANY WARRANTY; without even the implied warranty of              -
; - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               -
; - GNU General Public License for more details.                                -
; -                                                                             -
; - You should have received a copy of the GNU General Public License           -
; - along with this program; if not, write to the Free Software                 -
; - Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. -
; -------------------------------------------------------------------------------

; -------------------------------------------
; - Author:  Jean-Bernard HUET - JBHuet.com -
; - Created: 27 déc. 2021                   -
; -------------------------------------------

; -----------------------------------------------------------------------------------------------
; -                                                                                             -
; -   This INI file contains informations to connect to database used for Memory application.   -
; -   'embedded' aims local SQLite server ( = database file) located in application files.      -
; -   'local' aims local MySQL server on the current computer.                                  -
; -   'remote' aims remotely hosted (on the web) MySQL server.                                  -
; -                                                                                             -
; -   VERY IMPORTANT: this file MUST BE explicitely listed in project's .gitignore              -
; -                                                                                             -
; -   Section names must ALWAYS begin with 'embedded', 'local' or 'remote'.                     -
; -   User name and password sections names must ALWAYS end with access level                   -
; -     * 'root'                                                                                -
; -     * 'admin'                                                                               -
; -     * 'read_write'                                                                          -
; -     * 'write_only'                                                                          -
; -     * 'read_only' is the default value for 'local' and 'remote'                             -
; -     * 'not_used' is reserved for 'embedded' server and is the default value in this case    -
; -                                                                                             -
; -----------------------------------------------------------------------------------------------

; --------------------------------------------------------
; -                                                      -
; -   embedded database server connection informations   -
; -                                                      -
; -             Should be the default ones               -
; -                                                      -
; --------------------------------------------------------
[embedded_db_server]
driver = sqlite
host = "dbmemory.sqlite"

; -----------------------------------------------------
; -                                                   -
; -   local database server connection informations   -
; -                                                   -
; -           Should be the default ones              -
; -                                                   -
; -----------------------------------------------------
;[local_db_server]
;driver = mysql
;host = localhost
;port = 3306

; ------------------------------------------------------
; -                                                    -
; -   remote database server connection informations   -
; -                                                    -
; -               As given by hoster                   -
; -                                                    -
; ------------------------------------------------------
;[remote_db_server]
;driver = mysql
;host = localhost
;port = 3306

; ---------------------------------
; -                               -
; -   local database name         -
; -                               -
; -   Should be the default one   -
; -                               -
; ---------------------------------
;[local_db_schema]
;schema = local_memory_db_name

; ----------------------------
; -                          -
; -   remote database name   -
; -                          -
; -    As given by hoster    -
; -                          -
; ----------------------------
;[remote_db_schema]
;schema = hosted_memory_db_name

; --------------------------------------
; -                                    -
; -   local root server access level   -
; -                                    -
; -         VERY DANGEROUS!!!          -
; -            NEVER USE!!!            -
; -      DELETE those informations     -
; -                                    -
; --------------------------------------
;[local_db_root]
;username = root_if_not_renamed
;password = a_very_strong_password

; ---------------------------------------
; -                                     -
; -   hosted root server access level   -
; -                                     -
; -          VERY DANGEROUS!!!          -
; -            NEVER USE!!!             -
; -      DELETE those informations      -
; -                                     -
; ---------------------------------------
;[remote_db_root]
;username = root_if_not_renamed
;password = a_very_strong_password

; -------------------------------------------------
; -                                               -
; -   local database administrator access level   -
; -                                               -
; -           DANGEROUS for database!!!           -
; -             Use with great care!              -
; -        Consider restricting privileges        -
; -                                               -
; -------------------------------------------------
;[local_db_admin]
;username = db_admin_or_something_harder_to_guess
;password = a_very_strong_password

; --------------------------------------------------
; -                                                -
; -   hosted database administrator access level   -
; -                                                -
; -           DANGEROUS for database!!!            -
; -             Use with great care!               -
; -        Consider restricting privileges         -
; -                                                -
; -------------------------------------------------
;[remote_db_admin]
;username = db_admin_or_something_harder_to_guess
;password = a_very_strong_password

; --------------------------------------------------
; -                                                -
; -   local database read and write access level   -
; -                                                -
; -               Risks limited to:                -
; -                 * data loss                    -
; -                 * data corruption              -
; -                 * data breach                  -
; -                                                -
; --------------------------------------------------
;[local_db_read_write]
;username = memory_rw_or_another_explicit_name
;password = a_strong_password

; ---------------------------------------------------
; -                                                 -
; -   hosted database read and write access level   -
; -                                                 -
; -               Risks limited to:                 -
; -                 * data loss                     -
; -                 * data corruption               -
; -                 * data breach                   -
; -                                                 -
; ---------------------------------------------------
;[remote_db_read_write]
;username = memory_rw_or_another_explicit_name
;password = a_strong_password

; ----------------------------------------------
; -                                            -
; -   local database write only access level   -
; -                                            -
; -           Risks limited to:                -
; -             * data loss                    -
; -             * data corruption              -
; -                                            -
; ----------------------------------------------
;[local_db_write_only]
;username = memory_wo_or_another_explicit_name
;password = a_strong_password

; -----------------------------------------------
; -                                             -
; -   hosted database write only access level   -
; -                                             -
; -           Risks limited to:                 -
; -             * data loss                     -
; -             * data corruption               -
; -                                             -
; -----------------------------------------------
;[remote_db_write_only]
;username = memory_wo_or_another_explicit_name
;password = a_strong_password

; ---------------------------------------------
; -                                           -
; -   local database read only access level   -
; -                                           -
; -          Risks limited to:                -
; -            * data breach                  -
; -                                           -
; ---------------------------------------------
;[local_db_read_only]
;username = memory_ro_or_another_explicit_name
;password = a_strong_password

; ----------------------------------------------
; -                                            -
; -   hosted database read only access level   -
; -                                            -
; -          Risks limited to:                 -
; -            * data breach                   -
; -                                            -
; ----------------------------------------------
;[remote_db_read_only]
;username = memory_ro_or_another_explicit_name
;password = a_strong_password
