/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Author:  Jean-Bernard HUET - JBHuet.com
 * Created: 27 déc. 2021
 */

/**
 * t_current_game stocke les informations concernant la partie
 *
 * t_current_game est une table de type 'clé -> valeur'
 * qui permet de stocker n'importe quel type d'information sous forme de chaîne de caractères.
 *
 * id : identifiant unique de l'information
 * game_key : clé de l'information
 * game_value : valeur de l'information, sous forme de chaîne de caractères
 */
CREATE TABLE t_current_game (
    id         INTEGER PRIMARY KEY AUTOINCREMENT
                       UNIQUE
                       NOT NULL,
    game_key   VARCHAR UNIQUE
                       NOT NULL
                       CHECK (t_current_game.game_key <> ''),
    game_value VARCHAR NOT NULL
                       DEFAULT ('')
);
