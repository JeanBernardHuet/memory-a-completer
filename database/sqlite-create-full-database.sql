/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Author:  Jean-Bernard HUET - JBHuet.com
 */

--
-- Fichier généré par SQLiteStudio v3.3.3 sur lun. déc. 27 19:54:22 2021
--
-- Encodage texte utilisé : UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table : t_board_game
DROP TABLE IF EXISTS t_board_game;

CREATE TABLE t_board_game (
    id         INTEGER PRIMARY KEY AUTOINCREMENT
                       UNIQUE
                       NOT NULL,
    card_type  INTEGER NOT NULL
                       CHECK (t_board_game.card_type >= 0),
    card_order INTEGER NOT NULL
                       CHECK (t_board_game.card_order >= 0),
    pair_found BOOLEAN NOT NULL
                       DEFAULT (false)
);


-- Table : t_current_game
DROP TABLE IF EXISTS t_current_game;

CREATE TABLE t_current_game (
    id         INTEGER PRIMARY KEY AUTOINCREMENT
                       UNIQUE
                       NOT NULL,
    game_key   VARCHAR UNIQUE
                       NOT NULL
                       CHECK (t_current_game.game_key <> ''),
    game_value VARCHAR NOT NULL
                       DEFAULT ('')
);


-- Table : t_game_level
DROP TABLE IF EXISTS t_game_level;

CREATE TABLE t_game_level (
    id           INTEGER PRIMARY KEY AUTOINCREMENT
                         NOT NULL
                         UNIQUE,
    label        VARCHAR UNIQUE
                         NOT NULL
                         CHECK (t_game_level.label <> ''),
    max_duration INTEGER NOT NULL
                         CHECK (t_game_level.max_duration > 0)
);

INSERT INTO t_game_level (
                             id,
                             label,
                             max_duration
                         )
                         VALUES (
                             1,
                             'Facile',
                             15
                         );

INSERT INTO t_game_level (
                             id,
                             label,
                             max_duration
                         )
                         VALUES (
                             2,
                             'Moyen',
                             10
                         );

INSERT INTO t_game_level (
                             id,
                             label,
                             max_duration
                         )
                         VALUES (
                             3,
                             'Difficle',
                             5
                         );

INSERT INTO t_game_level (
                             id,
                             label,
                             max_duration
                         )
                         VALUES (
                             4,
                             'Impossible',
                             2
                         );


-- Table : t_games_history
DROP TABLE IF EXISTS t_games_history;

CREATE TABLE t_games_history (
    id         INTEGER  PRIMARY KEY AUTOINCREMENT
                        UNIQUE
                        NOT NULL,
    game_date  DATETIME NOT NULL
                        DEFAULT (DATE('now') )
                        CHECK (t_games_history.game_date >= DATE('now') ),
    game_level INTEGER  REFERENCES t_game_level (id) ON DELETE RESTRICT
                                                     ON UPDATE CASCADE
                        NOT NULL,
    duration   TIME     NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
