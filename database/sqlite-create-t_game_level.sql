/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Author:  Jean-Bernard HUET - JBHuet.com
 * Created: 27 déc. 2021
 */

/**
 * t_game_level stocke les différents niveaux de difficulté du jeu
 * id : identifiant unique du niveau de difficulté
 * label : nom du niveau de difficulté
 * max_duration : durée maximale d'une partie pour ce niveau de difficulté
 */
CREATE TABLE t_game_level (
    id           INTEGER PRIMARY KEY AUTOINCREMENT
                         NOT NULL
                         UNIQUE,
    label        VARCHAR UNIQUE
                         NOT NULL
                         CHECK (t_game_level.label <> ''),
    max_duration INTEGER NOT NULL
                         CHECK (t_game_level.max_duration > 0)
);

