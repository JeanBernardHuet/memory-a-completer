/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Author:  Jean-Bernard HUET - JBHuet.com
 * Created: 27 déc. 2021
 */

/**
 * t_board_game stocke les données relatives au jeu
 *
 * La liste des cartes comprend les informations sur le type de carte, l'ordre sur le plateau de jeu et si la paire a été trouvée.
 *
 * id : identifiant unique de la carte
 * card_type : type de carte
 * card_order : position de la carte dans la file déposée sur le plateau de jeu
 * pair_found : indique si la carte fait partie d'une paire trouvée
 */
CREATE TABLE t_board_game (
    id         INTEGER PRIMARY KEY AUTOINCREMENT
                       UNIQUE
                       NOT NULL,
    card_type  INTEGER NOT NULL
                       CHECK (t_board_game.card_type >= 0),
    card_order INTEGER NOT NULL
                       CHECK (t_board_game.card_order >= 0),
    pair_found BOOLEAN NOT NULL
                       DEFAULT (false)
);
