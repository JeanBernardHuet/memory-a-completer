/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * Author:  Jean-Bernard HUET - JBHuet.com
 * Created: 27 déc. 2021
 */

/**
 * t_games_history stocke les informations sur les parties gagnées
 *
 * id : identifiant unique d'une partie
 * game_date : date et heure de fin de la partie
 * game_level : niveau de la partie
 * duration : durée totale de la partie
 */
CREATE TABLE t_games_history (
    id         INTEGER  PRIMARY KEY AUTOINCREMENT
                        UNIQUE
                        NOT NULL,
    game_date  DATETIME NOT NULL
                        DEFAULT (DATE('now') )
                        CHECK (t_games_history.game_date >= DATE('now') ),
    game_level INTEGER  REFERENCES t_game_level (id) ON DELETE RESTRICT
                                                     ON UPDATE CASCADE
                        NOT NULL,
    duration   TIME     NOT NULL
);

