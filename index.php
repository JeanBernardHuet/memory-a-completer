<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
/*
 * Le code de toute l'application nécessite au minimum PHP 7.4
 *
 * La POSTURE de sécurité DE BASE est de ne faire confiance à PERSONNE !
 * Toute l'application va donc considérer que toutes les données qui lui sont externes
 * (saisies par l'utilisateur, stockées dans un fichier ou dans une base de données)
 * sont potentiellement dangereuses.
 * On va donc vérifier systématiquement toute information issue de l'extérieur de chaque script.
 */

// Pour améliorer la logique et la sécurité, on force le typage strict ("strong type") des variables : cela interdit le transtypage ("type casting") que PHP peut faire à notre insu.
declare(strict_types = 1);

include_once 'TGameManager.php';

$Game = new TGameManager();

if (isset($_POST['cardOrder'])) {

    $Game->storeStartGame();
    if ($Game->timeIsUp()) {
        $Response['endGame'] = 'outOfTime';
    }
    else {
        (array) $Response['cardOrder'] = filter_input(INPUT_POST, 'cardOrder',
                                                      FILTER_SANITIZE_NUMBER_INT);
        (string) $CurrentCardType       = $Game->checkTypeCard((intval($Response['cardOrder'])));
        $Response['cardType']  = $CurrentCardType;
        (array) $FirstCard             = $Game->readCardById($Game->firstCardId());
        if (isset($FirstCard[0]['card_type'])) {
            $Response['isPairFound'] = ($CurrentCardType == $FirstCard[0]['card_type']);
            if ($Response['isPairFound']) {
                $Game->storePairFound($Response['cardType']);
                $Response['isGameWon'] = $Game->isGameWon();
                if ($Response['isGameWon']) {
                    $Game->storeGameHistory();
                }
            }
            $Response['firstCardOrder'] = $FirstCard[0]['card_order'];
            $Game->resetFirstCard();
        }
        else {
            $Game->storeFirstCardId(intval($Response['cardOrder']));
        }
    }
    echo json_encode($Response);
}
else {
    $Game->newGame();
}
