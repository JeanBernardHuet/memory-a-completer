<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
declare(strict_types = 1);

/**
 * Description of TGamesHistory
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
class TGamesHistory {

    private \PDO $FDbManager;

    /**
     *
     * @param \PDO $ADbManager
     */
    public function __construct(\PDO $ADbManager) {
        $this->FDbManager = $ADbManager;
    }

    /**
     *
     * @param int $AGames
     * @param int $ALevel
     * @return array
     * @throws \Exception
     */
    public function readBestGamesFilteredByLevel(int $AGames = 3, int $ALevel): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT gh.game_date AS `Game date`, gl.label AS `Level`, gh.duration AS `Duration` FROM t_games_history AS gh JOIN t_game_level AS gl ON gh.game_level = gl.id WHERE gh.game_level = :level ORDER BY gh.game_date DESC LIMIT :games');
            $Request->bindValue(':games', $AGames, PDO::PARAM_INT);
            $Request->bindValue(':level', $ALevel, PDO::PARAM_INT);
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - SELECT gh.game_date AS `Game date`, gl.label AS `Level`, gh.duration AS `Duration` FROM t_games_history AS gh JOIN t_game_level AS gl ON gh.game_level = gl.id WHERE gh.game_level = :level ORDER BY gh.game_date DESC LIMIT :games' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not read n best games history filtered by level.');
            }
        }
        catch (\PDOException $pdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT gh.game_date AS `Game date`, gl.label AS `Level`, gh.duration AS `Duration` FROM t_games_history AS gh JOIN t_game_level AS gl ON gh.game_level = gl.id WHERE gh.game_level = :level ORDER BY gh.game_date DESC LIMIT :games. Error detail = ' . $pdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Error when reading n best games history filtered by level.');
        }
    }

    /**
     *
     * @param int $ALevel
     * @param \DateTime $ADuration
     * @return array
     * @throws \Exception
     */
    public function insertGameHistory(int $ALevel, \DateTime $ADuration): array {
        try {
            $Request = $this->FDbManager->prepare('INSERT INTO t_games_history (game_level, duration) VALUES (:level, :duration)');
            $Request->bindValue(':level', $ALevel, PDO::PARAM_INT);
            $Request->bindValue(':duration', $ADuration->format('H:i:s'));
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - INSERT INTO t_games_history () VALUES (:level, :duration)' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not insert new game history.');
            }
        }
        catch (\PDOException $pdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing INSERT INTO t_games_history () VALUES (:level, :duration). Error detail = ' . $pdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Error when inserting new game history.');
        }
    }

    /**
     *
     * @return bool
     * @throws \Exception
     */
    public function clean(): bool {
        try {
            $Request = $this->FDbManager->prepare('TRUNCATE TABLE t_games_history');
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing TRUNCATE TABLE t_games_history. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not clean games history table.');
        }
    }

}
