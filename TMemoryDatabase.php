<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/**
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
/*
 * PHP 7.4 is the minimum version to run the code below.
 *
 * As this class is designed to managed data coming from outside its own code, the correct posture is: TRUST NO ONE!
 */

// Strong typing is a good basis for security
declare(strict_types = 1);

include_once 'TDatabaseConnection.php';

/**
 * TDatabase class manages Memory game database
 *
 * TDatabase objects must be used to interact with Memory game database.
 * TDatabase class gets all the methods to connect to and to disconnect from Memory game database.
 * TDatabase class gets all the methods to manage CRUD.
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
class TMemoryDatabase extends TDatabaseConnection {

    /**
     * Creates connection to database
     *
     * @param string $DbServerParam Database server.
     *                              Correct values: 'embedded', 'local' and 'remote'.
     *                              Default value: 'embedded'.
     * @param string $AccessLevelParam Access level for the account used to access database.
     *                                 Correct values : 'root', 'admin', 'read_write', 'write_only', 'read_only', 'not_used'.
     *                                 Default value for 'embedded' server: 'not_used'.
     *                                 Default value for 'local' or 'remote' server: 'read_only'.
     */
    public function __construct(
            string $AIniFilePath = __DIR__ . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR,
            string $AIniFileName = 'MemoryDbSettings.ini',
            string $ADbServer = PARENT::SRV_EMBEDDED,
            string $AAccessLevel = PARENT::DB_NO
    ) {
        $this->connect($AIniFilePath, $AIniFileName, $ADbServer, $AAccessLevel);
    }

    /**
     * Initiates connection to Memory database
     *
     * Use connect() to initiate connection to the database.
     * Database connection will be setup accordingly to parameters found in INI settings file.
     * Un-comment code to use with another database server instead of SQlite.
     *
     * @param string $DbServerParam Database server type.
     *                              Correct values: PARENT::SRV_EMBEDDED, PARENT::SRV_LOCAL and PARENT::SRV_REMOTE.
     *                              Default value: PARENT::SRV_EMBEDDED.
     * @param string $AccessLevelParam Access level for the account used to connect to database.
     *                                 Correct values : PARENT::DB_ROOT, PARENT::DB_ADMIN, PARENT::DB_RW, PARENT::DB_WO, PARENT::DB_RO, PARENT::DB_NO.
     *                                 When database server type is PARENT::SRV_EMBEDDED, default value is PARENT::DB_NO.
     *                                 When database server type is PARENT::SRV_LOCAL or PARENT::SRV_REMOTE, default value is PARENT::DB_RO.
     * @return bool Connection status: 'True' means connection is enabled, 'False' means a problem occured and connection is NOT enabled.
     * @throws \Exception
     */
    public function connect(
            string $AIniFilePath = __DIR__ . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR,
            string $AIniFileName = 'MemoryDbSettings.ini',
            string $ADbServer = PARENT::SRV_EMBEDDED,
            string $AAccessLevel = PARENT::DB_NO
    ): bool {

        // Checking if $ADbServer is correct
        $this->checkFDbServerType($ADbServer);

        // Checking if $AAccessLevel is correct
        $this->checkFDbAccessLevel($AAccessLevel);

        // Resetting any existing connection
        $this->disconnect();

        // Reading INI file to associate each value to a key
        $this->fillDbConnectionFields($AIniFilePath, $AIniFileName);

        // Trying to connect to db
        try {
            if (PARENT::SRV_EMBEDDED == $this->FDbServerType) {
                if (PARENT::DB_NO == $this->FDbAccessLevel) {
                    $this->FDatabase = new \PDO($this->FDbDriver . ':' . __DIR__ . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . $this->FDbHost,
                                                null, null,
                                                array(
                        \PDO::ATTR_EMULATE_PREPARES   => false,
                        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
                    ));
                }
                else {
                    // to be defined
                }
            }
            else {
                $this->FDatabase = new \PDO(
                        $this->FDbDriver . ':dbname=' . $this->FDbSchema . ';host=' . $this->FDbHost,
                        $this->FDbUserName, $this->FDbUserPassword
                );
            }
        }
        catch (\PDOException $exceptionError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Unable to connect. Error detail = ' . $exceptionError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Database server unavailable.');
        }
        finally {
            // connect() always returns connection status
            $this->FIsConnected = ! (null == $this->FDatabase);
            return $this->FIsConnected;
        }
    }

}
