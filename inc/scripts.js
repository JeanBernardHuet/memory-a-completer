/*
 * Copyright (C) 2021 Jean-Bernard
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
var httpReq = new XMLHttpRequest();

function serverAnswerHandler() {
    serverAnswer = JSON.parse(httpReq.responseText);
    if (('cardOrder' in serverAnswer) && ('cardType' in serverAnswer)) {
        document.getElementById('c' + serverAnswer.cardOrder).className = "card visible type" + serverAnswer.cardType;
        document.getElementById('c' + serverAnswer.cardOrder).removeEventListener('click', sCdOd);
    }
    if ('isPairFound' in serverAnswer) {
        if (serverAnswer.isPairFound === false) {
            if ('firstCardOrder' in serverAnswer)
                setTimeout(() => {
                    document.getElementById('c' + serverAnswer.cardOrder).className = "card flipped";
                    document.getElementById('c' + serverAnswer.cardOrder).addEventListener('click', sCdOd);
                    document.getElementById('c' + serverAnswer.firstCardOrder).className = "card flipped";
                    document.getElementById('c' + serverAnswer.firstCardOrder).addEventListener('click', sCdOd);
                }, 3000);
        }
    }
    if ('isGameWon' in serverAnswer) {
        if (serverAnswer.isGameWon === true) {
            alert('Bravo, gagné !');
        }
    }
    if ('endGame' in serverAnswer) {
        if ('outOfTime' == serverAnswer.isGameWon) {
            alert('Le temps est écoulé. La partie est terminée...');
        }
    }
}

function sendCardOrder(cardOrder) {
    httpReq.onload = serverAnswerHandler;
    httpReq.open('POST', 'index.php', true);
    httpReq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    httpReq.send('cardOrder=' + cardOrder);
}

function sCdOd() {
    sendCardOrder(this.id);
}

var cardsList = document.getElementsByClassName('card');
numberOfCards = cardsList.length;
for (var i = 0; i < numberOfCards; i++) {
    cardsList[i].addEventListener('click', sCdOd);

}
