<?php

/*
 * Copyright (C) 2021 Jean-Bernard HUET - JBHuet.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
declare(strict_types = 1);

/**
 * Description of TBoardGame
 *
 * @author Jean-Bernard HUET - JBHuet.com <contact@jbhuet.com>
 * @licence GPL version 2
 * @copyright (c) 2021-2022, Jean-Bernard HUET - JBHuet.com
 */
class TBoardGame {

    private \PDO $FDbManager;

    /**
     *
     * @param \PDO $ADbManager
     */
    public function __construct(\PDO $ADbManager) {
        $this->FDbManager = $ADbManager;
    }

    /**
     * readFullList()returns the content of the t_board_game table, holding type, position and stauts of each card on the board.
     *
     * @return array
     * @throws \Exception
     */
    public function readFullList(): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_board_game');
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_board_game' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading full current game list request.');
            }
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_board_game. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read full board game table.');
        }
    }

    /**
     *
     * @return array
     * @throws \Exception
     */
    public function readFoundCards(): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_board_game WHERE pair_found = TRUE');
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_board_game WHERE pair_found = TRUE' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading full current game list request.');
            }
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_board_game WHERE pair_found = TRUE. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read found cards list from board game table.');
        }
    }

    /**
     *
     * @return array
     * @throws \Exception
     */
    public function readNotFoundCards(): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_board_game WHERE pair_found = FALSE');
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_board_game WHERE pair_found = FALSE' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading full current game list request.');
            }
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_board_game WHERE pair_found = FALSE. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read not found cards list from board game table.');
        }
    }

    /**
     *
     * @param int $AOrder
     * @return array
     * @throws \Exception
     */
    public function readCardByOrder(int $AOrder): array {
        try {
            $Request = $this->FDbManager->prepare('SELECT * FROM t_board_game WHERE card_order = :card_order');
            $Request->bindValue(':card_order', $AOrder, PDO::PARAM_INT);
            if ($Request->execute()) {
                return $Request->fetchAll();
            }
            else {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_board_game WHERE card_order = :card_order' . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not execute reading card by order request.');
            }
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_board_game WHERE card_order = :card_order. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not read card by order from board game table.');
        }
    }

    /**
     *
     * @param int $AId
     * @return array
     * @throws \Exception
     */
    public function readCardById(int $AId): array {
        if (0 > $AId) {
            return [
                    ];
        }
        else {
            try {
                $Request = $this->FDbManager->prepare('SELECT * FROM t_board_game WHERE id = :id');
                $Request->bindValue(':id', $AId, PDO::PARAM_INT);
                if ($Request->execute()) {
                    return $Request->fetchAll();
                }
                else {
                    $LogTime = new \DateTime('NOW');
                    error_log(
                            $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute SELECT * FROM t_board_game WHERE id = :id' . PHP_EOL,
                                             3,
                                             __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                    );
                    throw new \Exception('Can not execute reading card by id request.');
                }
            }
            catch (\PDOException $PdoError) {
                $LogTime = new \DateTime('NOW');
                error_log(
                        $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing SELECT * FROM t_board_game WHERE id = :id. Error detail = ' . $PdoError . PHP_EOL,
                                         3,
                                         __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                );
                throw new \Exception('Can not read card by id from board game table.');
            }
        }
    }

    /**
     *
     * @param int $AId
     * @param int $AValue
     * @return bool
     * @throws \Exception
     */
    public function updateCardTypeById(int $AId, int $AValue): bool {
        try {
            $Request = $this->FDbManager->prepare('UPDATE t_board_game SET card_type = :value WHERE id = :id');
            $Request->bindValue(':id', $AId, PDO::PARAM_INT);
            $Request->bindValue(':value', $AValue, PDO::PARAM_INT);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing UPDATE t_board_game SET card_type = :value WHERE id = :id. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not update card type by id.');
        }
    }

    /**
     *
     * @param int $AId
     * @param int $AValue
     * @return bool
     * @throws \Exception
     */
    public function updateCardOrderById(int $AId, int $AValue): bool {
        try {
            $Request = $this->FDbManager->prepare('UPDATE t_board_game SET card_order = :value WHERE id = :id');
            $Request->bindValue(':id', $AId, PDO::PARAM_INT);
            $Request->bindValue(':value', $AValue, PDO::PARAM_INT);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing UPDATE t_board_game SET card_order = :value WHERE id = :id. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not update card order by id.');
        }
    }

    /**
     *
     * @param int $AType
     * @param bool $AValue
     * @return bool
     * @throws \Exception
     */
    public function updatePairFoundByType(int $AType, bool $AValue): bool {
        try {
            $Request = $this->FDbManager->prepare('UPDATE t_board_game SET pair_found = :value WHERE card_type = :card_type');
            $Request->bindValue(':card_type', $AType, PDO::PARAM_INT);
            $Request->bindValue(':value', $AValue, PDO::PARAM_BOOL);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing UPDATE t_board_game SET pair_found = :value WHERE card_type = :card_type. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not update pair found value by type.');
        }
    }

    // delete card board game by id
    public function deleteCardById(int $AId): bool {
        try {
            $Request = $this->FDbManager->prepare('DELETE FROM t_board_game WHERE id = :id');
            $Request->bindValue(':id', $AId, PDO::PARAM_INT);
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing DELETE FROM t_board_game WHERE id = :id. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not delete card by id.');
        }
    }

    /**
     *
     * @return bool
     * @throws \Exception
     */
    public function clean(): bool {
        try {
            $Request = $this->FDbManager->prepare('DELETE FROM t_board_game');
            return $Request->execute();
        }
        catch (\PDOException $PdoError) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing TRUNCATE TABLE t_current_game. Error detail = ' . $PdoError . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not clean current game table.');
        }
    }

    /**
     *
     * @throws \Exception
     */
    public function newBoard(): void {
        (array) $CardsList = [
            0,
            0,
            1,
            1,
            2,
            2,
            3,
            3,
            4,
            4,
            5,
            5,
            6,
            6,
            7,
            7,
            8,
            8,
            9,
            9,
            10,
            10,
            11,
            11,
            12,
            12,
            13,
            13,
            14,
            14,
            15,
            15,
            16,
            16,
            17,
            17,];
        shuffle($CardsList);
        (int) $Order     = 0;
        try {
            $this->clean();
            $Request = $this->FDbManager->prepare('INSERT INTO t_board_game (card_type, card_order) VALUES (:type, :order)');
            foreach ($CardsList as $CardType) {
                $Request->bindValue(':type', $CardType, PDO::PARAM_INT);
                $Request->bindValue(':order', $Order, PDO::PARAM_INT);
                if ( ! $Request->execute()) {
                    $LogTime = new \DateTime('NOW');
                    error_log(
                            $LogTime->format(DateTime::COOKIE) . ' *** PDO Statement error - Unable to execute INSERT INTO t_board_game (card_type, card_order) VALUES (:type, :order)' . PHP_EOL,
                                             3,
                                             __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
                    );
                    throw new \Exception('Can not insert new card on board.');
                }
                $Order ++;
            }
        }
        catch (\Exception $Error) {
            $LogTime = new \DateTime('NOW');
            error_log(
                    $LogTime->format(DateTime::COOKIE) . ' *** Database error - Error when executing INSERT INTO t_board_game (card_type, card_order) VALUES (:type, :order). Error detail = ' . $Error . PHP_EOL,
                                     3,
                                     __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'errors.log'
            );
            throw new \Exception('Can not insert new card on board.');
        }
    }

}
